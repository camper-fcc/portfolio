[fCC Build a Portfolio Page Project](https://learn.freecodecamp.org/responsive-web-design/responsive-web-design-projects/build-a-personal-portfolio-webpage)

[Project](https://camper-fcc.gitlab.io/portfolio)

[Code](https://gitlab.com/camper-fcc/portfolio)

[fCC Forum Post](https://www.freecodecamp.org/forum/t/portfolio-project-complete-feedback-requested/161542)

Some features of the project:

- All native CSS, using flexbox and media queries – no Bootstrap, JavaScript, jQuery, etc...
- Responsive & mobile first design (Mostly - let me know what doesn't work)
- Fixed header
- [No inline styles](https://www.thoughtco.com/avoid-inline-styles-for-css-3466846)
- [No CSS ID selectors for styling](http://screwlewse.com/2010/07/dont-use-id-selectors-in-css/)
- Uses [semantic HTML](https://internetingishard.com/html-and-css/semantic-html/)
- Passes [FCC Beta tests](https://beta.freecodecamp.org/en/challenges/applied-responsive-web-design-projects/build-a-personal-portfolio-webpage)
- Hosted on [GitLab Pages](https://about.gitlab.com/features/pages/)
- Includes Favicon
- Uses [Julius Sans One](https://fonts.google.com/specimen/Julius+Sans+One) and [Crimson Text](https://fonts.google.com/specimen/Crimson+Text) Google Fonts -- [Font combination found here](https://www.behance.net/gallery/35768979/Typography-Google-Fonts-Combinations)
- Uses [Normalize.css](https://necolas.github.io/normalize.css/)
- Links to [Font Awesome CDN](https://www.bootstrapcdn.com/fontawesome/)
- Uses [viewport units](https://beta.freecodecamp.org/en/challenges/responsive-web-design-principles/make-typography-responsive)
- Uses [optimized images](https://images.guide/)
- Valid [HTML](https://validator.w3.org/nu/?showsource=yes&showoutline=yes&showimagereport=yes&doc=https%3A%2F%2Fcamper-fcc.gitlab.io%2Fportfolio) and [CSS](https://jigsaw.w3.org/css-validator/#validate_by_input)

Thanks in advance for your time reviewing my project! :sunny: